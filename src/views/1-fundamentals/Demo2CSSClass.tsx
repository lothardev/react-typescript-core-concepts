import React from "react"

export const DemoCSSClass: React.FC = () => {
  return (
    <div className="example-box-centered small">
      <Panel
        title="hello react"
        text="Custom colors"
        headerClass="bg-dark text-white"
        bodyClass="bg-info"
      />
      <br/>
      <Panel text="default colors"/>
    </div>
  )
};

// =========================================
// PANEL SIMPLE COMPONENT WITH CSS CLASSES
// =========================================

interface PanelProps {
  title?: string;
  text?: string;
  headerClass?: string
  bodyClass?: string
}

export const Panel: React.FC<PanelProps> = props => {
  const cssHeader = `card-header ${props.headerClass || 'bg-secondary'}`;
  const cssBody = `card-body ${props.bodyClass}`;

  return (
    <div className="card">
      { props.title && <div className={cssHeader}>{props.title}</div> }
      <div className={cssBody}>{props.text}</div>
    </div>
  )
};
