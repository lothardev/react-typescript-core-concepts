import { Product } from '../../shared/model/product';

type Action = |
  { type: 'SET_ACTIVE', payload: Product }
  ;

export function activeReducer(state: Product, action: Action) {
  switch (action.type) {
    case 'SET_ACTIVE':
      return action.payload;
  }
  return state;
}
