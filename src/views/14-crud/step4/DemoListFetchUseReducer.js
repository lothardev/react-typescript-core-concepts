import React, {useRef} from 'react';

import useCrud from './useCrud'

//////////////////////////////////////////////////////////////////////////

function crudReducer(state, action) {
  switch (action.type) {
    case 'init_start':
      return {
        ...state,
        initLoading: true
      };
    case 'init_success':
      return {
        ...state,
        initLoading: false,
        error: null,
        data: action.data
      };
    case 'init_failure':
      return {
        ...state,
        initLoading: false,
        error: action.error
      };
    case 'start':
      return {
        ...state,
        loading: true
      };

    // ADD actions
    case 'add_success':
      return {
        ...state,
        loading: false,
        data: [...state.data, action.data]
      };
    case 'add_failure':
      return {
        ...state,
        loading: false,
        error: action.error
      };

    // DELETE actions
    case 'delete_success':
      return {
        ...state,
        loading: false,
        data: state.data.filter(item => item.id !== action.data.id)
      };
    case 'delete_failure':
      return {
        ...state,
        loading: false,
        error: action.error
      };

    default:
      throw new Error('Invalid action type.')
  }
}


///////////////////////////////////////////////////////////////////////////////

export function DemoListFetchUseReducer() {
  const nameRef = useRef('')
  const emailRef = useRef('')
  const [{initLoading, loading, error, data}, REST] = useCrud(
    'https://jsonplaceholder.typicode.com/users', crudReducer
  );

  const onSubmit = (e) => {
    e.preventDefault()
    console.log(nameRef.current.value, emailRef.current.value)
    REST.addItem('https://jsonplaceholder.typicode.com/users', {
      name: nameRef.current.value,
      email: emailRef.current.value,
      userId: 10
    })
      .then(() => {
        nameRef.current.value = '';
        emailRef.current.value = '';
      })
  };

  const onDelete = (item) => {
    console.log(item)
    REST.deleteItem(`https://jsonplaceholder.typicode.com/users/${item.id}`, item)
  };

  console.log({initLoading, loading, error, data})
  return (
    <div className="App">
      {initLoading
        ? <p>Loading data...</p>
        : (<React.Fragment>
          <form onSubmit={onSubmit}>
            <input type='text' name='name' ref={nameRef}/>
            <input type='text' name='email' ref={emailRef}/>
            <button type='submit' disabled={loading}>Submit</button>
          </form>

          {loading && <p>Waiting server response...</p>}
          {error && <p>{error}</p>}
          {data.map(post => (
            <div key={post.id}>
              <div>#{post.id} - {post.name} <button onClick={() => onDelete(post)}>X</button></div>
              <p>{post.email}</p>
            </div>
          ))}
        </React.Fragment>)}
    </div>
  );
}
